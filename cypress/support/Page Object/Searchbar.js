import Wait from "../../support/Page Object/waits";
const waitFor = new Wait()
class SearchBar{
    getPrintableForms(){
        return cy.get('[href="https://qa-35.twoca.org/api/apps/Printable-forms/index.html"]')
    }
    getMSFReports(){
        cy.get('[href="https://qa-35.twoca.org/api/apps/MSF-Reports/index.html"]').click()

    }
}
export default SearchBar