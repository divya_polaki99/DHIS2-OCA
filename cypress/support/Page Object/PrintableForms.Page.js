class PrintableForms{
    getDropDownBar(){
        return cy.get('[data-test=dhis2-uicore-select-input]', {timeout:6000})
    }
    getSearchBar(){
        return cy.get('#filter')
    }
    getSingleTallySheet(){
        return cy.get('input[name="Tally sheet - Adolescent Sexual Health Haiti"]')
    }
    getMultipleTallySheets(value){
        let locator
        locator='input[name=\"' + value + "\"]"
        return cy.get(locator)
    }
    getLineList(){
        return cy.get('input[name="Line-list - ~ IPD admissions - routine data"]')
    }
    getDropDownButton(){
        return cy.get('#icon/16/arrow-down')
    }
    getUICore(){
        return cy.get('[data-test=dhis2-uicore-layer]')
    }
    getApplyChanges(){
        return cy.get('button.jsx-2371629422:nth-child(3)')
    }
}
export default PrintableForms