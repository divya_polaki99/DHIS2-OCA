import Wait from "../../support/Page Object/waits";
const waitFor = new Wait()
class MSFReportsPage{
    loadProjectReport(projectTemplate,year,month){
        cy.get('[data-test=dhis2-uicore-select-input]').click()
        cy.contains(projectTemplate).click()
        cy.get('.MuiInputBase-root').click()
        cy.get(':nth-child(1) > .MuiButton-label > .MuiTypography-root').click()
        cy.contains(year).click() 
        cy.contains(month).click() 
        cy.contains('Load the Report').click()
        waitFor.waitTill(1500)
    }
    loadMissionReport(missionTemplate,year,month){
        cy.get('[data-test=dhis2-uicore-select-input]').click()
        cy.contains(missionTemplate).click()
        cy.get('.MuiInputBase-root').click()
        cy.get(':nth-child(1) > .MuiButton-label > .MuiTypography-root').click()
        cy.contains(year).click() 
        cy.contains(month).click() 
        cy.contains('Load the Report').click()
    }
    // getReportTemplate(){
    //     cy.get('[data-test=dhis2-uicore-select-input]').click()
    // }
    // getProjectTemplate(projectTemplate){
    //     cy.contains(projectTemplate).click()
    // }
    // getMissionTemplate(missionTemplate){
    //     cy.contains(missionTemplate).click()
    // }
    // pickThePeriod(year, month){
    //     cy.get('.MuiInputBase-root').click()
    //     cy.get(':nth-child(1) > .MuiButton-label > .MuiTypography-root').click()
    //     cy.contains(year).click() 
    //     cy.contains(month).click()    
    // }
    selectProject(project){
        cy.contains(project).click()
    }
    selectMission(mission){
        cy.contains(mission).click()
    }
     loadReport(){
         cy.contains('Load the Report').click()
         waitFor.waitTill(1500)
     }
    exportReport(){
        cy.get('.export-to-word').click()
        waitFor.waitTill(1000)
    }
    checkSection(){
        cy.get('input[name="Mental Health"]').uncheck({force: true})
    }
    checkSectionMis(){
        return cy.get('input[name="Context - where, when, what"]')
    }
    narrativeOverview(){
        return cy.xpath("//div[@class='data-element']/child::h5[text()='AREP_005 MMR narrative author section \"Overview\" ']/following-sibling::input")
    }
    narrativeKeyAchievements(){
        return cy.xpath("//div[@class='data-element']/child::h5[text()='AREP_001 MMR narrative key achievements']/following-sibling::textarea")
    }
    narrativeContext(){
        return cy.xpath("//div[@class='data-element']/child::h5[text()='AREP_025 MMR narrative author section \"Context\"']/following-sibling::input")
    }
    populationMovements(){
        return cy.xpath("//div[@class='data-element']/child::h5[text()='AREP_050 MMR context: Population movements']/following-sibling::textarea")
    }
    staffingIssues(){
        return cy.xpath("//div[@class='data-element']/child::h5[text()='AREP_054 MMR context: Staffing issues']/following-sibling::textarea")
    }
    patientMortality(){
        return cy.xpath("//div[@class='data-element']/child::h5[text()='AREP_079 MMR patient mortality']/following-sibling::textarea")
    }
    narrativeRHGynobs(){
        return cy.xpath("//div[@class='data-element']/child::h5[text()='AREP_008 MMR narrative RHGynobs']/following-sibling::textarea")
    }
    specificPharmacy(){
        return cy.xpath("//div[@class='data-element']/child::h5[text()='AREP_077 MMR specific objective pharmacy/supply']/following-sibling::textarea")
    }
    narrativeOther(){
        return cy.xpath("//div[@class='data-element']/child::h5[text()='AREP_024 MMR narrative other']/following-sibling::textarea")
    }
    narrativeKeyAchievementsMission(){
        return cy.xpath("//div[@class='data-element']/child::h5[text()='AREP_100 Mission MMR narrative key achievements']/following-sibling::textarea")
    }
    narrativeFuturePlansMission(){
        return cy.xpath("//div[@class='data-element']/child::h5[text()='AREP_102 Mission MMR narrative future plans']/following-sibling::textarea")
    }
    accessChangesMission(){
        return cy.xpath("//div[@class='data-element']/child::h5[text()='AREP_108 Mission MMR context: Access changes']/following-sibling::textarea")
    }
    logisticProblemsMission(){
        return cy.xpath("//div[@class='data-element']/child::h5[text()='AREP_110 Mission MMR context: Supply /logistic problems']/following-sibling::textarea")
    }
    reportTemplate(){
        return cy.get('.report-template')
    }
    helpToAddChartOrTable(){
        cy.get('.help-text > a').click()
        cy.get('h1[data-test=dhis2-uicore-modaltitle]').should('have.text', 'Adding tables and charts to report templates')
        cy.get('button[class="jsx-2371629422 primary"]').click()
    }
    reportCode(){
        return cy.get('div.section-wrapper:nth-child(2) > h3:nth-child(1) > span:nth-child(3)')
    }

}
export default MSFReportsPage