class OrgUnitStructure{
    getMSF(){
        return cy.get('[data-test=dhis2-uiwidgets-orgunittree-node-toggle] > .jsx-4223269773')
    }
    getOCA(){
        return cy.get('.open > :nth-child(1) > [data-test=dhis2-uiwidgets-orgunittree-node-toggle] > .jsx-4223269773')
    }
    getOCX(){
        return cy.get(':nth-child(2) > [data-test=dhis2-uiwidgets-orgunittree-node-toggle] > .jsx-4223269773')
    }
    getAfg(){
        return cy.get(':nth-child(2) > :nth-child(1) > :nth-child(2) > .open > :nth-child(1) > [data-test=dhis2-uiwidgets-orgunittree-node-toggle] > .jsx-4223269773')
    }
    getBangladesh(){
        return cy.get(':nth-child(2) > :nth-child(1) > :nth-child(2) > .open > :nth-child(2) > [data-test=dhis2-uiwidgets-orgunittree-node-toggle] > .jsx-4223269773')
    }
    getCAR(){
        return cy.get(':nth-child(3) > [data-test=dhis2-uiwidgets-orgunittree-node-toggle] > .jsx-4223269773')
    }
    getNorthKivu(){
        return cy.get(':nth-child(4) > [data-test=dhis2-uiwidgets-orgunittree-node-toggle] > .jsx-4223269773')
    }
    getSouthKivu(){
        return cy.get(':nth-child(5) > [data-test=dhis2-uiwidgets-orgunittree-node-toggle] > .jsx-4223269773')
    }
    getEthiopia(){
        return cy.get(':nth-child(6) > [data-test=dhis2-uiwidgets-orgunittree-node-toggle] > .jsx-4223269773')
    }
    getHaiti(){
        return cy.get(':nth-child(7) > [data-test=dhis2-uiwidgets-orgunittree-node-toggle] > .jsx-4223269773')
    }
    getIndia(){
        return cy.get(':nth-child(8) > [data-test=dhis2-uiwidgets-orgunittree-node-toggle] > .jsx-4223269773')
    }
    getIraq(){
        return cy.get(':nth-child(9) > [data-test=dhis2-uiwidgets-orgunittree-node-toggle] > .jsx-4223269773')
    }
    getJordan(){
        return cy.get(':nth-child(10) > [data-test=dhis2-uiwidgets-orgunittree-node-toggle] > .jsx-4223269773')
    }
    getLibya(){
        return cy.get(':nth-child(11) > [data-test=dhis2-uiwidgets-orgunittree-node-toggle] > .jsx-4223269773')
    }
    getMalaysia(){
        return cy.get(':nth-child(12) > [data-test=dhis2-uiwidgets-orgunittree-node-toggle] > .jsx-4223269773')
    }
    getMozambique(){
        return cy.get(':nth-child(13) > [data-test=dhis2-uiwidgets-orgunittree-node-toggle] > .jsx-4223269773')
    }
    getMyanmar(){
        return cy.get(':nth-child(14) > [data-test=dhis2-uiwidgets-orgunittree-node-toggle] > .jsx-4223269773')
    }
    getNigeria(){
        return cy.get(':nth-child(15) > [data-test=dhis2-uiwidgets-orgunittree-node-toggle] > .jsx-4223269773')
    }
    getPak(){
        return cy.get(':nth-child(16) > [data-test=dhis2-uiwidgets-orgunittree-node-toggle] > .jsx-4223269773')
    }
    getSierra(){
        return cy.get(':nth-child(17) > [data-test=dhis2-uiwidgets-orgunittree-node-toggle] > .jsx-4223269773')
    }
    getSomalia(){
        return cy.get(':nth-child(18) > [data-test=dhis2-uiwidgets-orgunittree-node-toggle] > .jsx-4223269773')
    }
    getSouthSudan(){
        return cy.get(':nth-child(19) > [data-test=dhis2-uiwidgets-orgunittree-node-toggle] > .jsx-4223269773')
    }
    getUzb(){
        return cy.get(':nth-child(20) > [data-test=dhis2-uiwidgets-orgunittree-node-toggle] > .jsx-4223269773')
    }
    getYemen(){
        return cy.get(':nth-child(21) > [data-test=dhis2-uiwidgets-orgunittree-node-toggle] > .jsx-4223269773')
    }
}
export default OrgUnitStructure