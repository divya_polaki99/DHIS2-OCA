
import Wait from "../../support/Page Object/waits";
const waitFor = new Wait()
class PivotTable{
    favourites(){
        waitFor.waitTill(2000)
        cy.get('#button-1556').click()
    }
    open(){
        cy.contains('Open').click()
        waitFor.waitTill(2000)
    }
    search(table){
        cy.get('input[placeholder="Search for favorites.."]').type(table)
        cy.get('#Rmj7p9k1qcI').click()//click searched table
        waitFor.waitTill(2000)
    }
    saveAs(newTable){
        cy.contains('Save as').click()//save as button
        cy.get('input[placeholder="Unnamed"]',{timeout:6000}).clear({force:true}).type(newTable)//save as field
        cy.xpath('//button[span[text()="Save"]]').click()
    }
    searchAndDelete(table){
        cy.get('input[placeholder="Search for favorites.."]').type(table)//search for saved table
        waitFor.waitTill(1000)
        cy.get('.x-action-col-icon.x-action-col-2.ns-grid-row-icon-delete.tooltip-favorite-delete').click()//delete icon
        cy.contains('OK').click()//ok button in delete popup
    }
}
export default PivotTable