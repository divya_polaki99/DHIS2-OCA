class OpenOCAOrgUnit{
    openOCA(){
        cy.contains('MSF')
        cy.get('span.jsx-4223269773').click()
        cy.contains('OCA')
        cy.get('span.jsx-4223269773').eq(1).click()
    }
    openMission(){
        cy.xpath('(//span[text()="Afghanistan"]/ancestor::div[contains(@class, "node")])[3]/div[@data-test="dhis2-uiwidgets-orgunittree-node-toggle"]').click()
    }
}
export default OpenOCAOrgUnit