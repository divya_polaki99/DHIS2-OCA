import Header from "../../support/Page Object/Header";
import LoginPage from "../../support/Page Object/Login.Page";
import MSFReportsPage from "../../support/Page Object/MSFReports.Page";
import OpenOCAOrgUnit from "../../support/Page Object/OpenOCAOrgUnit";
import PivotTable from "../../support/Page Object/PivotTable";
import SearchBar from "../../support/Page Object/Searchbar";
import Wait from "../../support/Page Object/waits";

describe('Test MSF Reports', function(){
    const fs = require('fs')
    let pivotTable = 'MMR-Mission-sample'
    let code='P1027_REP_MMR02_1-GEN'
    let profile
    let year='2019', month='Sep', appName='MSF Reports'
    const loginPage=new LoginPage()
    const header = new Header()
    const searchBar = new SearchBar()
    const msfReports = new MSFReportsPage()
    const openOca = new OpenOCAOrgUnit()
    const waitFor = new Wait()
    const pivot = new PivotTable()
    let months =  new Map([['Jan','01'],['Feb', '02'], ['Mar','03'], ['Apr','04'], ['May','05'], ['Jun','06'], ['Jul','07'], ['Aug','08'],['Sep','09'],['Oct','10'],['Nov','11'],['Dec','12']])
    let project = 'Helmand Secondary Healthcare', mission = 'Ethiopia'
    let projectTemplate = 'Monthly medical report - project', missionTemplate = 'Monthly medical report - mission'
    let projectDocFile= projectTemplate+'_'+project+'_'+year+'-'+months.get(month)+'.docx'
    let missionDocFile= missionTemplate+'_'+mission+'_'+year+'-'+months.get(month)+'.docx'
    let projectFilePath = 'cypress/downloads/'+projectDocFile
    let missionFilePath = 'cypress/downloads/'+missionDocFile
    before(function(){
        cy.fixture('profile').then(function(testdata){
            profile = testdata
        })
    })
    this.beforeEach(function(){
        cy.visit(Cypress.config("baseUrl"))
        loginPage.getUsername().type(profile.adminUsername)
        loginPage.getPassword().type(profile.adminPassword)
        loginPage.getSigninButton().click()
        cy.viewport(1300,1200)
        waitFor.waitTill(5000)
        header.getApps()
        header.getSearchBar().type(appName)
        waitFor.waitTill(1000)
        searchBar.getMSFReports()
    })
    it('[Project]Testing the selectability of a program for an org unit and export the report', function(){
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false
        })
        waitFor.waitTill(2000)
        openOca.openOCA()
        openOca.openMission()
        msfReports.selectProject(project)
        msfReports.loadProjectReport(projectTemplate,year,month)
        msfReports.exportReport()
        cy.readFile(projectFilePath).should('exist')
        cy.exec('node cypress/support/deleteProjectReports.js')
    })
    it('[Project]Asserting on unchecked sections', function(){
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false
        })
        waitFor.waitTill(2000)
        openOca.openOCA()
        openOca.openMission()
        msfReports.selectProject(project)
        msfReports.loadProjectReport(projectTemplate,year,month)
        msfReports.checkSection()
        msfReports.loadReport() 
        cy.get('.report-template').should('not.contain.text', 'Mental Health')
    })
    it('[Project]Report data fill- Export report', function(){
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false
        })
        
        waitFor.waitTill(2000)
        openOca.openOCA()
        openOca.openMission()
        msfReports.selectProject(project)
        msfReports.loadProjectReport(projectTemplate,year,month)
        cy.fixture('msfdata').then((msfData)=>{
            msfReports.narrativeOverview().clear().type(msfData.narrativeOverview)
            msfReports.narrativeKeyAchievements().clear().type(msfData.narrativeKeyAckeivements)
            msfReports.narrativeContext().clear().type(msfData.narrativeContext)
            msfReports.staffingIssues().clear().type(msfData.staffingIssues)
            msfReports.patientMortality().clear().type(msfData.patientMortality)
            msfReports.narrativeRHGynobs().clear().type(msfData.narrativeRHGynobs)
            msfReports.specificPharmacy().clear().type(msfData.specificPharmacy)
            msfReports.narrativeOther().clear().type(msfData.narrativeOther)
        })
        
        msfReports.exportReport()
        waitFor.waitTill(2000)
        cy.readFile(projectFilePath).should('exist') 
        cy.exec('node cypress/support/deleteProjectReports.js')
    })
    it('[Mission]Testing the selectability of a program for an org unit and export the report', function(){
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false
        })
        waitFor.waitTill(2000)
        openOca.openOCA()
        msfReports.selectMission(mission)
        msfReports.loadMissionReport(missionTemplate,year,month)
        msfReports.exportReport()
        cy.readFile(missionFilePath).should('exist')     
        cy.exec('node cypress/support/deleteMissionReports.js')    
    })
    it('[Mission]Asserting on unchecked sections', function(){
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false
        })
        waitFor.waitTill(2000)
        openOca.openOCA()
        msfReports.selectMission(mission)
        msfReports.loadMissionReport(missionTemplate,year,month)
        msfReports.checkSectionMis().uncheck({force: true})
        msfReports.loadReport()  
        msfReports.reportTemplate().should('not.contain.text', 'Context - where, when, what')
    })
    it('[Mission]Report data fill- Export report', function(){
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false
        })
        waitFor.waitTill(2000)
        openOca.openOCA()
        msfReports.selectMission(mission)
        msfReports.loadMissionReport(missionTemplate,year,month)  
        cy.fixture('msfdata').then((msfData)=>{
            msfReports.narrativeKeyAchievementsMission().clear().type(msfData.narrativeKeyAchievementsMission)
            msfReports.narrativeFuturePlansMission().clear().type(msfData.narrativeOverview)
            msfReports.accessChangesMission().clear().type(msfData.accessChangesMission)
            msfReports.logisticProblemsMission().clear().type(msfData.logisticProblemsMission)
        })
        msfReports.exportReport()
        cy.readFile(missionFilePath).should('exist') 
        cy.exec('node cypress/support/deleteMissionReports.js')
    })
    it('Help for adding a table or chart', function(){
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false
        })
        waitFor.waitTill(2000)
        openOca.openOCA()
        openOca.openMission()
        msfReports.selectProject(project)
        msfReports.loadProjectReport(projectTemplate,year,month)
        msfReports.helpToAddChartOrTable()

    })
    it('Adding a table/chart to the reports', function(){
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false
        })
        waitFor.waitTill(2000)
        openOca.openOCA()
        msfReports.selectMission(mission)
        msfReports.loadMissionReport(missionTemplate,year,month)
        msfReports.reportCode().invoke('text').then((text1)=>{
            expect(text1).to.eq(code)
        })
        cy.visit(Cypress.config("baseUrl")+"/dhis-web-pivot/")
        pivot.favourites()
        pivot.open()
        pivot.search(pivotTable)
        pivot.favourites()
        pivot.saveAs(code+'-1')
        cy.visit(Cypress.config("baseUrl")+"/api/apps/MSF-Reports/index.html")
        waitFor.waitTill(1500)
        openOca.openOCA()
        msfReports.selectMission(mission)
        msfReports.loadMissionReport(missionTemplate,year,month)
        waitFor.waitTill(500)
        cy.get('.pivot-filter').should('be.visible')//assert on saved table
        cy.visit(Cypress.config("baseUrl")+"/dhis-web-pivot/")
        pivot.favourites()
        pivot.open()
        pivot.searchAndDelete(code)    
    })
})