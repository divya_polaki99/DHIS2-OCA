import Header from "../../support/Page Object/Header";
import LoginPage from "../../support/Page Object/Login.Page"
import PrintableForms from "../../support/Page Object/PrintableForms.Page";
import SearchBar from "../../support/Page Object/Searchbar";
import Wait from "../../support/Page Object/waits";

describe('Test Printable forms', function(){
    let profile
    const loginPage = new LoginPage()
    const header = new Header()
    const searchBar= new SearchBar()
    const printableForms = new PrintableForms()
    const waitFor = new Wait()
    before(function(){
        cy.fixture('profile').then(function(testdata){
            profile=testdata
        })
    })
   this.beforeEach(function(){
        cy.visit(Cypress.config("baseUrl"))
        loginPage.getUsername().type(profile.adminUsername)
        loginPage.getPassword().type(profile.adminPassword)
        loginPage.getSigninButton().click()
        cy.viewport(1300, 1200) // Set viewport to 550px x 750px
   })
    it('Testing the selectability of a single tally sheets', function(){
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false
        })
        waitFor.waitTill(10000)
        header.getApps().click({force: true})
        header.getSearchBar().type('Printable Forms')
        searchBar.getPrintableForms().click()
        waitFor.waitTill(10000)
        printableForms.getDropDownBar().click()
        printableForms.getSearchBar().type('tally')
        printableForms.getSingleTallySheet().check({force:true})
        printableForms.getUICore().click()
        printableForms.getApplyChanges().click()
       })
       it('Testing the selectability of a multiple tally sheets', function(){
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false
        })
        let tallySheetNames=["Tally sheet - Adolescent Sexual Health Haiti","Tally sheet - General OPD (Emergency) Afghanistan - Helmand","Tally sheet - General OPD Myanmar"]
        waitFor.waitTill(10000)
        header.getApps().click({force: true})
        header.getSearchBar().type('Printable Forms')
        searchBar.getPrintableForms().click()
        printableForms.getDropDownBar().click()
        printableForms.getSearchBar().type('tally')
        for(let i=0;i<tallySheetNames.length;i++){
            printableForms.getMultipleTallySheets(tallySheetNames[i]).check({force:true})
        }
        printableForms.getUICore().click()
        printableForms.getApplyChanges().click()
       })
       it('Testing the selectability of a line-list', function(){
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false
        })
        waitFor.waitTill(10000)
        header.getApps().click({force: true})
        header.getSearchBar().type('Printable Forms')
        searchBar.getPrintableForms().click()
        printableForms.getDropDownBar().click()
        printableForms.getSearchBar().type('line')
        printableForms.getLineList().check({force:true})
        printableForms.getUICore().click()
        printableForms.getApplyChanges().click()
       })
})